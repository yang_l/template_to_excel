package com.example.template_excel.listener.interfacemethod;

import com.alibaba.excel.util.ListUtils;
import com.example.template_excel.domain.ExcelUser;
import com.example.template_excel.dto.ExcelUserDTO;
import com.example.template_excel.mapper.ExcelUserMapper;
import com.example.template_excel.mapstruct.MapStructConvert;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author YangLi
 * @Date 2025/1/7 14:09
 * @注释
 */
@Component
public class ExcelUserDTOHandler implements SaveDataHandler<ExcelUserDTO, ExcelUser> {

    @Resource
    private ExcelUserMapper excelUserMapper;

    @Override
    public void saveData(List<ExcelUser> afterConvertList) {
        excelUserMapper.batchInsert(afterConvertList);
    }

    @Override
    public List<ExcelUser> convertClass(List<ExcelUserDTO> list, int batchCount) {
        ArrayList<ExcelUser> userList = ListUtils.newArrayListWithCapacity(batchCount);
        list.forEach(excelUserDTO -> {
            MapStructConvert instance = MapStructConvert.INSTANCE;
            userList.add(instance.excelUserDTO2ExcelUser(excelUserDTO));
        });
        return userList;
    }
}

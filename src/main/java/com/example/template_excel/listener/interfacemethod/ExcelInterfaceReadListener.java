package com.example.template_excel.listener.interfacemethod;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.read.listener.ReadListener;
import com.alibaba.excel.util.ListUtils;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.example.template_excel.listener.extendmethd.ExcelUserDtoExtendsListener;
import lombok.Data;

import java.util.List;

/**
 * @Author YangLi
 * @Date 2025/1/7 13:49
 * @注释
 */
@Data
public class ExcelInterfaceReadListener<K, V> implements ReadListener<K> {

    /**
     * 单次读取的数量 int 类型属于常量池里面的东西  所以可以直接使用 static final 修饰
     */
    protected static final int BATCH_COUNT = 100;

    // 默认阈值 使用无参构造时 我们默认满 100 就插入一次数据
    private int batchCount = 100;

    private final SaveDataHandler<K, V> saveDataHandler;

    public ExcelInterfaceReadListener(SaveDataHandler<K, V> saveDataHandler) {
        this.saveDataHandler = saveDataHandler;
    }

    // 如果使用有参 则使用用户传入的 batchCount
    public ExcelInterfaceReadListener(int batchCount, SaveDataHandler<K, V> saveDataHandler) {
        this.batchCount = batchCount;
        this.saveDataHandler = saveDataHandler;
    }

    /**
     * 搞一个初始大小为 100 的 list 来存放我们单次读取的数据
     */
    private List<K> list = ListUtils.newArrayListWithCapacity(BATCH_COUNT);

    /**
     * @param k               读取 excel 每一行用什么类型来接收
     * @param analysisContext 上下文信息
     */
    @Override
    public void invoke(K k, AnalysisContext analysisContext) {
        list.add(k);
        // 每读取一行就判断 list 是否达到了批量插入的数据量 如果是就插入数据库
        if (list.size() >= batchCount) {
            saveData();
            // 存储完成清理 list
            list = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
        }
    }

    /**
     * excel 读完之后 会调用这个方法
     *
     * @param analysisContext 上下文信息
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        if (!CollectionUtils.isEmpty(list)) {
            saveData();
            list = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
        }
    }

    /* 在读取到了数据之后 我们需要将其插入到数据库 但是不同的 excel 会使用不同的类接受*/
    /* 举个例子：读取 Student.xlsx 时 我们会创建和 student 相关的类来接收里面的数据 */
    /*         读取 teacher.xlsx 时 我们会创建和 teacher 相关的类来接收里面的数据 */
    /*         读取 user.xlsx 时 我们会创建和 user 相关的类来接收里面的数据 */
    /* 那么我们这里不知道具体需要使用哪一个 mapper 来插入数据
                        插入 student 需要 studentMapper
                        插入 teacher 需要 teacherMapper
                        插入 user 需要 userMapper 所以我们使用抽象类+抽象方法的 形式 让子类自己去写saveData的方法 */

    /**
     * 这里使用 {@link ExcelUserDtoExtendsListener} 为例
     * <p>
     * /**
     * 加上存储数据库 由字类自己去实现
     */
     private void saveData(){
         // 数据插入前 需要转换成mapper能使用的 BO 比如 我们使用 xxxDTO 读取到了数据之后 需要将 xxxDTO 转成 xxxBO 来入库
         List<V> afterConvertList = saveDataHandler.convertClass(list, batchCount);
         saveDataHandler.saveData(afterConvertList);
     }

}


package com.example.template_excel.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.template_excel.domain.ExcelUser;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author YangLi
 * @Date 2025/1/6 14:03
 * @注释
 */
@Mapper
public interface ExcelUserMapper extends BaseMapper<ExcelUser> {

    void batchInsert(List<ExcelUser> list);

}

package com.example.template_excel.controller;

import com.example.template_excel.response.ResultResponse;
import com.example.template_excel.service.ExcelReadService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author YangLi
 * @Date 2025/1/6 10:39
 * @注释
 */
@RestController
@RequestMapping("/excel-read")
public class ExcelReadController {

    @Resource
    private ExcelReadService excelReadService;

    /**
     * 简单的读
     */
    @PostMapping("/simpleRead")
    public ResultResponse<String> simpleRead(){
        return excelReadService.simpleRead();
    }

    /**
     * 指定列的下标或者列名的读
     */
    @PostMapping("/indexOrNameRead")
    public ResultResponse<String> indexOrNameRead() {
        return excelReadService.indexOrNameRead();
    }

    /**
     * 读多个或者全部sheet,这里注意一个sheet不能读取多次，多次读取需要重新读取文件
     */
    @PostMapping("/repeatedRead")
    public ResultResponse<String> repeatedRead() {
        return excelReadService.repeatedRead();
    }

    /**
     * 日期、数字或者自定义格式转换
     */
    @PostMapping("/converterRead")
    public ResultResponse<String> converterRead() {
        return excelReadService.converterRead();
    }

    /**
     * 读多行头
     */
    @PostMapping("/header-reader")
    public ResultResponse<String> headerRead() {
        return excelReadService.headerRead();
    }
}

package com.example.template_excel.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;

/**
 * @Author YangLi
 * @Date 2023/12/12 15:08
 * @注释
 */
@Slf4j
public class FileUtil {

    private FileUtil() {
        // 私有化工具类的构造函数
    }

    // 将file转为multiPartFile
    public static MultipartFile convertFileToMultipartFile(File file) throws IOException {
        try (FileInputStream fileInputStream = new FileInputStream(file)) {
            return new MockMultipartFile(
                    file.getName(),      // 文件名
                    file.getName(),      // 原始文件名
                    null,     // 文件类型
                    fileInputStream
            );
        }
    }

    // 读取文件内容为字符串
    public static String readFileToString(String filePath) throws IOException {
        StringBuilder content = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = reader.readLine()) != null) {
                content.append(line).append("\n");
            }
        }
        return content.toString();
    }

    // 写入字符串内容到文件
    public static void writeStringToFile(String filePath, String content) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath))) {
            writer.write(content);
        }
    }

    // 复制文件
    public static void copyFile(String sourcePath, String destinationPath) throws IOException {
        try (InputStream inputStream = new FileInputStream(new File(sourcePath));
             OutputStream outputStream = new FileOutputStream(new File(destinationPath))) {
            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }
        }
    }

    // 移动文件
    public static void moveFile(String sourcePath, String destinationPath) throws IOException {
        copyFile(sourcePath, destinationPath);
        deleteFile(sourcePath);
    }

    // 删除文件
    public static void deleteFile(String filePath) {
        File file = new File(filePath);
        if (file.exists()) {
            if (file.delete()) {
                log.info("文件删除成功");
            }else {
                log.info("文件删除失败");
            }
        }else {
            log.info("文件不存在，删除失败");
        }
    }

    // 获取文件大小
    public static long getFileSize(String filePath) {
        File file = new File(filePath);
        return file.length();
    }

    // 检查文件是否存在
    public static boolean fileExists(String filePath) {
        File file = new File(filePath);
        return file.exists();
    }

    // 将文件转为 byte[]
    public static byte[] convertFileToBytes(File file) throws IOException {
        try (FileInputStream fis = new FileInputStream(file);
             ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = fis.read(buffer)) != -1) {
                bos.write(buffer, 0, bytesRead);
            }
            return bos.toByteArray();
        }
    }
}

package com.example.template_excel.dto;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @Author YangLi
 * @Date 2025/1/8 18:18
 * @注释
 */
@Data
public class StudentIndexOrNameReadDTO {

    @ExcelIgnore
    private Long id;

    /**
     * @ExcelProperty() 不要index 和 value 一起用  例如： @ExcelProperty(value = "姓名", index = 0) 不要这样用
     * 要么使用index = 0 要么使用 value = "姓名"
     */
    @ExcelProperty(index = 0)
    private String name;

    @ExcelProperty(index = 2)
    private String sex;

    @ExcelProperty("生日")
    private LocalDateTime birthday;

    @ExcelProperty("班级")
    private String clazz;

    /**
     * @ExcelIgnore 这个注解就表示忽略某一个字段  当然也可以直接删掉这个字段
     * 这里写上主要是为了学习这个注解
     */
    @ExcelIgnore
    private BigDecimal score;
}

package com.example.template_excel.listener.extendmethd;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.exception.ExcelDataConvertException;
import com.alibaba.excel.metadata.data.ReadCellData;
import com.alibaba.excel.read.listener.ReadListener;
import com.alibaba.excel.util.ListUtils;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;

/**
 * @Author YangLi
 * @Date 2025/1/7 13:49
 * @注释
 */
@Data
@Slf4j
public abstract class ExcelReadExtendsListener<K, V> implements ReadListener<K> {

    /**
     * 单次读取的数量 int 类型属于常量池里面的东西  所以可以直接使用 static final 修饰
     * 这个BATCH_COUNT 字类进行类型转换的时候需要用到 所以我们使用 protected 修饰 好让字类能够访问
     */
    protected static final int BATCH_COUNT = 100;

    // 默认阈值 使用无参构造时 我们默认满 100 就插入一次数据
    protected int batchCount = 100;

    protected ExcelReadExtendsListener() {
    }

    // 如果使用有参 则使用用户传入的 batchCount
    protected ExcelReadExtendsListener(int batchCount) {
        this.batchCount = batchCount;
    }

    /**
     * 搞一个初始大小为 10 的 list 来存放我们单次读取的数据
     */
    private List<K> list = ListUtils.newArrayListWithCapacity(BATCH_COUNT);

    /**
     * 用这个存放头部信息  integer 表示第几列 从0开始  ReadCellData 类中 有一个叫 StringValue 的属性 这个属性就是头部信息
     */
    private Map<Integer, ReadCellData<?>> headMap;

    /**
     * @param k               读取 excel 每一行用什么类型来接收
     * @param analysisContext 上下文信息
     */
    @Override
    public void invoke(K k, AnalysisContext analysisContext) {
        list.add(k);
        // 每读取一行就判断 list 是否达到了批量插入的数据量 如果是就插入数据库
        if (list.size() >= batchCount) {
            // 对数据进行类型转换
            List<V> afterConvertList = convertClass(list, batchCount);
            saveData(afterConvertList);
            // 存储完成清理 list
            list = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
        }
    }

    /**
     * excel 读完之后 会调用这个方法
     *
     * @param analysisContext 上下文信息
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        if (!CollectionUtils.isEmpty(list)) {
            List<V> afterConvertList = convertClass(list, batchCount);
            saveData(afterConvertList);
            // 存储完成后清理 list
            list = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
        }
    }

    @Override
    public void invokeHead(Map<Integer, ReadCellData<?>> headMap, AnalysisContext context) {
        this.headMap = headMap;
    }

    /**
     * 重写这个方法， 会在出现异常的时候执行这个方法，根据自己的需求 去继续读 还是抛异常，
     * 也可以将具体失败的 行信息 记录到数据库中 后面修改后接着处理
     * @param exception ·
     * @param context ·
     */
    @Override
    public void onException(Exception exception, AnalysisContext context) {
        log.error("解析失败，但是继续解析下一行:{}", exception.getMessage());
        // 如果是某一个单元格的转换异常 能获取到具体行号
        // 如果要获取头的信息 配合invokeHeadMap使用
        if (exception instanceof ExcelDataConvertException) {
            ExcelDataConvertException excelDataConvertException = (ExcelDataConvertException)exception;
            log.error("第{}行，第{}列解析异常，数据为:{}，头为：{}", excelDataConvertException.getRowIndex(),
                    excelDataConvertException.getColumnIndex(), excelDataConvertException.getCellData()
            , headMap.get(excelDataConvertException.getColumnIndex()).getStringValue());
        }
    }

    /* 在读取到了数据之后 我们需要将其插入到数据库 但是不同的 excel 会使用不同的类接受*/
    /* 举个例子：读取 Student.xlsx 时 我们会创建和 student 相关的类来接收里面的数据 */
    /*         读取 teacher.xlsx 时 我们会创建和 teacher 相关的类来接收里面的数据 */
    /*         读取 user.xlsx 时 我们会创建和 user 相关的类来接收里面的数据 */
    /* 那么我们这里不知道具体需要使用哪一个 mapper 来插入数据
                        插入 student 需要 studentMapper
                        插入 teacher 需要 teacherMapper
                        插入 user 需要 userMapper 所以我们使用抽象类+抽象方法的 形式 让子类自己去写saveData的方法 */

    /**
     * 这里使用 {@link ExcelUserDtoExtendsListener} 为例
     * <p>
     * /**
     * 加上存储数据库 由子类自己去实现
     */
    protected abstract void saveData(List<V> afterConvertList);

    protected abstract List<V> convertClass(List<K> list, int batchCount);
}

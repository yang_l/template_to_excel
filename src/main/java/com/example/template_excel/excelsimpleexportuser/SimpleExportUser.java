package com.example.template_excel.excelsimpleexportuser;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @Author YangLi
 * @Date 2024/12/26 11:37
 * @注释
 */
@Data
public class SimpleExportUser {

    /**
     * @ExcelProperty  这个注解用来指定标题  这里值为 “用户名” 那么生成的 excel 标题就会有 用户名
     */
    @ExcelProperty(value = "用户名")
    private String username;

    @ExcelProperty(value = "密码")
    private String password;

    /**
     * @ExcelIgnore 这个注解用来表示忽略某个字段，这里在 address 这个字段加上这个字段  那么标题和数据都不会有 address
     */
    @ExcelIgnore
    @ExcelProperty(value = "地址")
    private String address;

    @ExcelProperty(value = "性别")
    private String sex;
}

package com.example.template_excel.dto;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.NumberFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @Author YangLi
 * @Date 2025/1/7 17:12
 * @注释
 */
@Data
public class StudentDTO {

    @ExcelIgnore
    private Long id;

    @ExcelProperty(value = "姓名")
    private String name;

    @ExcelProperty(value = "性别")
    private String sex;

    @ExcelProperty("生日")
    private LocalDateTime birthday;

    @ExcelProperty(value = "班级")
    private String clazz;

    @NumberFormat("#.##")
    @ExcelProperty(value = "成绩")
    private BigDecimal score;
}

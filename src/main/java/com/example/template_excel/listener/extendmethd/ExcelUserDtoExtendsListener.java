package com.example.template_excel.listener.extendmethd;

import com.alibaba.excel.util.ListUtils;
import com.example.template_excel.domain.ExcelUser;
import com.example.template_excel.dto.ExcelUserDTO;
import com.example.template_excel.mapper.ExcelUserMapper;
import com.example.template_excel.mapstruct.MapStructConvert;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author YangLi
 * @Date 2025/1/7 14:09
 * @注释
 */
@EqualsAndHashCode(callSuper = false)
public class ExcelUserDtoExtendsListener extends ExcelReadExtendsListener<ExcelUserDTO, ExcelUser> {
                                                                    // 可以发现这个地方 我将 K, V 都写死了
                                                                    // 意思就是这个 类只能处理 ExcelUserDTO + ExcelUser 的这种情况
                                                                    // 如果 有另外一个 ExcelUserNewDTO 接收 excel中的参数 并且需要转成 ExcelUser 入库的情况
                                                                    // 需要重写一个子类 继承 ExcelReadExtendsListener 来实现
                                                                    /**
                                                                     * {@link StudentDtoExtendsListener} 这个抽象类中 我只固定了 V  , K由调用方传递
                                                                     * 并且由调用方自己实现抽象方法 convertClass 会更加灵活
                                                                     */
    private final ExcelUserMapper excelUserMapper;

    public ExcelUserDtoExtendsListener(int batchCount, ExcelUserMapper excelUserMapper) {
        super(batchCount);
        this.excelUserMapper = excelUserMapper;
    }

    @Override
    protected void saveData(List<ExcelUser> afterConvertList) {
        excelUserMapper.batchInsert(afterConvertList);
    }

    @Override
    protected List<ExcelUser> convertClass(List<ExcelUserDTO> list, int batchCount) {
        ArrayList<ExcelUser> excelUserList = ListUtils.newArrayListWithExpectedSize(batchCount);
        list.forEach(excelUserDTO -> {
            MapStructConvert instance = MapStructConvert.INSTANCE;
            excelUserList.add(instance.excelUserDTO2ExcelUser(excelUserDTO));
        });
        return excelUserList;
    }
}

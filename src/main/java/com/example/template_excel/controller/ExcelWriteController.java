package com.example.template_excel.controller;

import com.example.template_excel.annotation.TimeConsuming;
import com.example.template_excel.response.ResultResponse;
import com.example.template_excel.service.ExcelWriteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
/**
 * @Author YangLi
 * @Date 2023/12/11 14:22
 * @注释
 */
@Slf4j
@RestController
@RequestMapping("/excel-write")
public class ExcelWriteController {

    @Resource
    private ExcelWriteService excelService;

    /**
     * 导出class中的属性列  被忽略的列直接跳过
     *
     * @return ·
     */
    @TimeConsuming
    @GetMapping("simpleExport")
    public ResultResponse<String> simpleExport() {
        return excelService.simpleExport();
    }

    /**
     * 导出class中的属性列  被忽略的列空出来
     *
     * @return ·
     */
    @TimeConsuming
    @GetMapping("appointRowExport")
    public ResultResponse<String> appointRowExport() {
        return excelService.appointRowExport();
    }

    /**
     * 导出class中的属性列  复杂标题的情况
     *
     * @return ·
     */
    @TimeConsuming
    @GetMapping("complexHeadExport")
    public ResultResponse<String> complexHeadExport() {
        return excelService.complexHeadExport();
    }

    /**
     * 导出class中的属性列  重复多次写入到 同一个 sheet 页中
     *
     * @return ·
     */
    @TimeConsuming
    @GetMapping("repeatSameSheetExport")
    public ResultResponse<String> repeatSameSheetExport() {
        return excelService.repeatSameSheetExport();
    }

    /**
     * 导出class中的属性列  重复多次写入到 不同 sheet 页中
     *
     * @return ·
     */
    @TimeConsuming
    @GetMapping("repeatDifferentSheetExport")
    public ResultResponse<String> repeatDifferentSheetExport() {
        return excelService.repeatDifferentSheetExport();
    }

    /**
     * 导出class中的属性列  主要是 日期 和 数字的转换
     *
     * @return ·
     */
    @TimeConsuming
    @GetMapping("converterDataExport")
    public ResultResponse<String> converterDataExport() {
        return excelService.converterDataExport();
    }

    /**
     * 写数据的时候 按自己创建的风格写
     * 比如 字体风格，颜色，大小，背景颜色，行高，列宽等
     *
     * @return ·
     */
    @TimeConsuming
    @GetMapping("writeByCustomerStyle")
    public ResultResponse<String> writeByCustomerStyle() {
        return excelService.writeByCustomerStyle();
    }

    /**
     * 按模版导出  包含多个 sheet 页的导出 以及 图片的插入
     *
     * @return ·
     */
    @TimeConsuming
    @GetMapping("templateExport")
    public ResultResponse<String> exportExcel() {
        return excelService.exportExcel();
    }

    /**
     * 模版导出  包含关于横向导出的示例 纵向 及其他相关的数据导出
     *
     * @return ·
     */
    @TimeConsuming
    @GetMapping("templateExportAnother")
    public ResultResponse<String> templateExport() {
        return excelService.templateExport();
    }

    /**
     * 模版导出  并发 模版导出性能测试
     *
     * @return ·
     */
    @TimeConsuming
    @GetMapping("concurrentTemplateExportAnother")
    public ResultResponse<String> concurrentTemplateExportAnother() {
        return excelService.concurrentTemplateExportAnother();
    }

    /**
     * 实际导出的情况 可能会有很高的并发 我们如果不做限制  那么有可能会吧内存干爆
     */
    @GetMapping("actualCombatExport")
    public ResultResponse<String> actualCombatExport() {
        return excelService.actualCombatExport();
    }
    // 本文参考 easyExcel 的官方文档： https://easyexcel.opensource.alibaba.com/docs/current/quickstart/fill
}



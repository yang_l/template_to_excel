package com.example.template_excel.service.impl;

import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.enums.WriteDirectionEnum;
import com.alibaba.excel.metadata.data.WriteCellData;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.alibaba.excel.util.ListUtils;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.alibaba.excel.write.metadata.fill.FillConfig;
import com.alibaba.excel.write.metadata.fill.FillWrapper;
import com.example.template_excel.domain.ExcelUser;
import com.example.template_excel.domain.Student;
import com.example.template_excel.dto.StudentStyleWrite;
import com.example.template_excel.excelappointrowexport.AppointRowExportUser;
import com.example.template_excel.domain.ExcelInfo;
import com.example.template_excel.domain.User;
import com.example.template_excel.excelcomplexheadexport.ExcelComplexHeadExport;
import com.example.template_excel.excelconverterdata.ConverterDataExport;
import com.example.template_excel.excelrepeatdifferentsheetexport.ExcelRepeatDifferentSheetExport;
import com.example.template_excel.excelsimpleexportuser.SimpleExportUser;
import com.example.template_excel.exceltemplateexport.ExportTimeAndTotal;
import com.example.template_excel.exceltemplateexport.TemplateExport;
import com.example.template_excel.repeatsamesheetexport.ExcelRepeatSameSheetExport;
import com.example.template_excel.response.ResultResponse;
import com.example.template_excel.service.ExcelWriteService;
import com.example.template_excel.utils.FileUtil;
import com.example.template_excel.utils.ImageUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;

/**
 * @Author YangLi
 * @Date 2024/12/26 11:15
 * @注释
 */
@Slf4j
@Service
public class ExcelWriteServiceImpl implements ExcelWriteService {

    @Value("${excel.address}")
    private String address;

    @Value("${excel.reportName}")
    private String reportName;

    @Value("${excel.picturePath}")
    private String picturePath;

    @Value("${excel.exportPath}")
    private String exportPath;

    private static final String XLSX = ".xlsx";

    private static final String SHEET_NAME = "sheet0";

    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    /**
     * 构建模板中带入数据
     *
     * @return ·
     */
    private List<User> createUser() {
        ArrayList<User> list = new ArrayList<>();
        list.add(new User("yangL", "1234567", "成都青羊区", "男"));
        list.add(new User("yangL", "1234567", "成都青羊区", "男"));
        return list;
    }

    /**
     * 结果图 {@link com\example\template_excel\excelsimpleexportuser\结果图.jpg}
     *
     * @return `
     */
    @Override
    public ResultResponse<String> simpleExport() {
        String fileName = exportPath + UUID.randomUUID() + XLSX;
        // 这里 需要指定写用哪个class去写，然后写到第几个sheet页中(不指定的话默认第一个sheet 页  注意：sheet 页从0开始)，sheet 页名字为 "第一页"  然后文件流会自动关闭
        // class 主要是用来写入标题的 和指定哪一些字段需要被忽略
        // doWrite() 这个方法会自动关闭流  并不需要调用finish()
        EasyExcelFactory.write(fileName, SimpleExportUser.class)
                // 如果这里想使用03 则 传入excelType = ExcelTypeEnum.XLS 参数即可 默认使用的是 07 版本后的 excel
                .excelType(ExcelTypeEnum.XLSX)
                .sheet(0, "第一页")
                // 这里提供数据  this::createUsers 这个lamda 表达式 表示使用 本类中的 createUsers() 方法返回的数据
                .doWrite(createUsers(SimpleExportUser.class, 1000000));

        // 我这里本想测试 2百万数据导出大概需要多长时间 我写在了同一个sheet中
        // 发现07版本后的excel最多只能写入 1048576行 16,384列数据
        // 03 版本会更少 65536 行 256列  那就先测试 1百万条数据的耗时
        // 更多的数据导出测试 后面写多个 sheet 页的时候再测试

        return ResultResponse.success();
    }

    /**
     * 结果图 {@link com\example\template_excel\excelappointrowexport\结果图.jpg}
     *
     * @return `
     */
    @Override
    public ResultResponse<String> appointRowExport() {
        String fileName = exportPath + UUID.randomUUID() + XLSX;
        // doWrite() 这个方法会自动关闭流  并不需要调用finish()
        EasyExcelFactory.write(fileName, AppointRowExportUser.class)
                // 如果这里想使用03 则 传入excelType = ExcelTypeEnum.XLS 参数即可 默认使用的是 07 版本后的 excel
                .excelType(ExcelTypeEnum.XLSX)
                .sheet(0, "第一页")
                // 这里提供数据  this::createUsers 这个lamda 表达式 表示使用 本类中的 createUsers() 方法返回的数据
                .doWrite(createUsers(AppointRowExportUser.class, 1000000));
        return ResultResponse.success();
    }

    /**
     * 结果图 {@link com\example\template_excel\excelcomplexheadexport\结果图.jpg}
     *
     * @return `
     */
    @Override
    public ResultResponse<String> complexHeadExport() {
        String fileName = exportPath + UUID.randomUUID() + XLSX;
        // doWrite() 这个方法会自动关闭流  并不需要调用finish()
        EasyExcelFactory.write(fileName, ExcelComplexHeadExport.class)
                // 如果这里想使用03 则 传入excelType = ExcelTypeEnum.XLS 参数即可 默认使用的是 07 版本后的 excel
                .excelType(ExcelTypeEnum.XLSX)
                .sheet(0, "第一页")
                // 这里提供数据  this::createUsers 这个lamda 表达式 表示使用 本类中的 createUsers() 方法返回的数据
                .doWrite(createUsers(AppointRowExportUser.class, 1000000));
        return ResultResponse.success();
    }

    /**
     * 结果图 {@link com\example\template_excel\repeatsamesheetexport\结果图.jpg}
     *
     * @return `
     */
    @Override
    public ResultResponse<String> repeatSameSheetExport() {
        String fileName = exportPath + UUID.randomUUID() + XLSX;
        // 先整一个 excelWriter excelWriter 本质也是写入流 那么既然是流 就需要在使用完毕之后关闭 可以在用完之后 调用 finish() 方法
        ExcelWriter excelWriter = EasyExcelFactory.write(fileName, ExcelRepeatSameSheetExport.class).excelType(ExcelTypeEnum.XLSX).build();
        // 在整一个 sheet 页 表示你要写到这个 sheet 中
        WriteSheet writeSheet = EasyExcelFactory.writerSheet(0, SHEET_NAME).build();
        // 然后直接开始写  这里模拟写5次 都重复写入同一个 sheet 页中
        for (int i = 0; i < 5; i++) {
            // 获取数据 这里循环5次  每次生成 200000 条数据 太大要超出一个 sheet 页的最大条数
            List<ExcelRepeatSameSheetExport> users = createUsers(ExcelRepeatSameSheetExport.class, 200000);
            // 每次写都传入的同一个 writeSheet 表示写到同一个 sheet 页中
            excelWriter.write(users, writeSheet);
        }
        // 关闭流
        excelWriter.finish();
        return ResultResponse.success();
    }

    /**
     * 不指定 head 结果图 {@link com\example\template_excel\excelrepeatdifferentsheetexport\不指定头部结果图.jpg}
     * 指定 head 结果图 {@link com\example\template_excel\excelrepeatdifferentsheetexport\指定头部结果图.jpg}
     *
     * @return `
     */
    @Override
    public ResultResponse<String> repeatDifferentSheetExport() {
        String fileName = exportPath + UUID.randomUUID() + XLSX;
        // excelWriter 本质也是写入流 那么既然是流 就需要在使用完毕之后关闭
        try (ExcelWriter excelWriter = EasyExcelFactory.write(fileName, ExcelRepeatDifferentSheetExport.class).excelType(ExcelTypeEnum.XLSX).build()) {
            for (int i = 0; i < 5; i++) {
                WriteSheet writeSheet = EasyExcelFactory.writerSheet(i, "sheet" + i).build();
                // 获取数据 这里循环5次  每次生成 200000 条数据
                List<ExcelRepeatSameSheetExport> users = createUsers(ExcelRepeatSameSheetExport.class, 200000);
                excelWriter.write(users, writeSheet);
            }
        }

        // 上面那个try里面生成的excel 每个sheet页的头部都一样
        // 那如何让每一个头部都不一样呢？

        try (ExcelWriter excelWriterDifferentHead = EasyExcelFactory.write(fileName, ExcelRepeatDifferentSheetExport.class).excelType(ExcelTypeEnum.XLSX).build()) {
            // 这里构建一个数组  每个元素都是 sheet 页生成头部所需的class
            Class<?>[] array = {SimpleExportUser.class, AppointRowExportUser.class, ExcelComplexHeadExport.class, ExcelRepeatSameSheetExport.class, ExcelRepeatDifferentSheetExport.class};
            for (int i = 0; i < 5; i++) {
                // 这里我们在创建 sheet 页的时候 通过 head() 方法来指定 写入头部的 clazz
                WriteSheet writeSheet = EasyExcelFactory.writerSheet(i, "sheet" + i).head(array[i]).build();
                // 获取数据 这里循环5次  每次生成 200000 条数据 太大要超出一个 sheet 页的最大条数
                List<?> users = createUsers(array[i], 200000);
                excelWriterDifferentHead.write(users, writeSheet);
            }
        }
        return ResultResponse.success();
    }

    /**
     * 结果图 {@link com\example\template_excel\excelconverterdata\结果图.jpg}
     *
     * @return `
     */
    @Override
    public ResultResponse<String> converterDataExport() {
        String fileName = exportPath + UUID.randomUUID() + XLSX;
        EasyExcelFactory.write(fileName, ConverterDataExport.class)
                .excelType(ExcelTypeEnum.XLSX)
                .sheet(0, SHEET_NAME)
                .doWrite(createUsers(ConverterDataExport.class, 100));
        return ResultResponse.success();
    }

    @Override
    public ResultResponse<String> exportExcel() {
        ClassPathResource classPathResource = new ClassPathResource("doc\\excelTemplate.xlsx");
        String fileName = UUID.randomUUID().toString() + "_" + "ExcelUser.xlsx";
        String downloadPath = exportPath + fileName;
        try (InputStream inputStream = classPathResource.getInputStream();
             // 这个是文件的输出流  意思是将生成的 excel 写入文件中 本次测试选择写入文件
             FileOutputStream out = new FileOutputStream(downloadPath)
             // 这个是 response 的输出流 意思是直接将生成的 excel 通过流的方式返给前端
             // ServletOutputStream resOut = response.getOutputStream()
        ) {
            // 存入图片
            File file = new File(picturePath);
            WriteCellData<Void> voidWriteCellData = ImageUtils.imageCells(FileUtil.convertFileToMultipartFile(file).getBytes());
            // 构建第一个 sheet 页数据
            ExcelInfo excelInfo = new ExcelInfo(address, reportName, "aa", DateFormatUtils.format(new Date(), "yyyy" + "年" + "MM" + "月" + "dd" + "日"), "test", voidWriteCellData);

            // 构建 ExcelWriterSheetBuilder 将数据写入 out 流中
            ExcelWriter excelWriter = EasyExcelFactory.write(out).withTemplate(inputStream).excelType(ExcelTypeEnum.XLSX).build();
            // 构建sheet页  注意 easyExcel 并不支持模板导出的方式直接修改 sheetName 即使你在这里设置了 sheetName 也还是会使用模板中读到的 sheetName
            WriteSheet writeSheet1 = EasyExcelFactory.writerSheet().sheetNo(0).build();
            WriteSheet writeSheet2 = EasyExcelFactory.writerSheet().sheetNo(1).build();
            // 往模板中填充数据 取值模板为  {filed}
            excelWriter.fill(excelInfo, writeSheet1);
            //设置创建行的方式
            FillConfig fillConfig = FillConfig.builder().forceNewRow(Boolean.TRUE).build();
            FillWrapper data = new FillWrapper("data", createUser());
            // 将data中的数据填充到模板   这个excel是一部分一部分填充的   每一个不同的部分都需要重新填充
            excelWriter.fill(data, fillConfig, writeSheet2);
            // 将所有的数据全部填充完再调用这个方法
            excelWriter.finish();

            // 使用 POI 修改 sheet 名称  这里注意 sheet 页的名称也有长度限制且不能重复 不然会抛出异常，导致 sheet 页名称设置失败
            try (FileInputStream fileInputStream = new FileInputStream(downloadPath)) {
                Workbook workbook = new XSSFWorkbook(fileInputStream);
                // 修改 sheet 名称
                workbook.setSheetName(0, "face");
                workbook.setSheetName(1, "test");
                try (FileOutputStream fileOut = new FileOutputStream(downloadPath)) {
                    workbook.write(fileOut);
                    workbook.close();
                }
            }
            return ResultResponse.success();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultResponse.error(e.getMessage());
        }
    }

    /**
     * 结果图 {@link com\example\template_excel\exceltemplateexport\结果图.jpg}
     * <p>
     * 经过测试 3w 条数据大概需要1分钟
     * 经过测试 5w 条数据大概需要3分钟30秒 这种耗时基本是不可接受的
     *
     * @return `
     */
    @Override
    public ResultResponse<String> templateExport() {
        log.info("开始导出: {}", LocalDateTime.now());
        String fileName = exportPath + UUID.randomUUID() + XLSX;
        File file = new File(fileName);
        ClassPathResource classPathResource = new ClassPathResource("doc\\template.xlsx");
        try (InputStream inputStream = classPathResource.getInputStream()) {
            // 将模版中的数据读取到流中 并写入到 file 中
            ExcelWriter excelWriter = EasyExcelFactory.write(file).withTemplate(inputStream).excelType(ExcelTypeEnum.XLSX).build();
            // 我们要填入的 sheet 页
            WriteSheet writeSheet = EasyExcelFactory.writerSheet(0, SHEET_NAME).build();
            // 横向填充的 fillConfig
            FillConfig fillConfigColumn = FillConfig.builder().direction(WriteDirectionEnum.HORIZONTAL).build();
            // 纵向填充的 fillConfig
            FillConfig fillConfigRow = FillConfig.builder().forceNewRow(Boolean.TRUE).build();
            // 需要插入的数据
            List<TemplateExport> users = createUsers(TemplateExport.class, 10000);
            // 横向填充
            FillWrapper fillWrapperColumn = new FillWrapper("userColumn", users.subList(0, 20));
            excelWriter.fill(fillWrapperColumn, fillConfigColumn, writeSheet);
            // 纵向填充
            FillWrapper fillWrapperRow = new FillWrapper("userRow", users);
            excelWriter.fill(fillWrapperRow, fillConfigRow, writeSheet);
            // 还有一些其他的数据 比如导出时间和统计总数
            ExportTimeAndTotal exportTimeAndTotal = new ExportTimeAndTotal(LocalDateTime.now(), (long) users.size());
            excelWriter.fill(exportTimeAndTotal, writeSheet);
            excelWriter.finish();
            log.info("导出完成： {}", LocalDateTime.now());
            return ResultResponse.success();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultResponse.error(e.getMessage());
        }
    }

    /**
     * 并发导出  每个表 5w条数据时  耗时5分钟15秒
     * 上面我们测试 单线程导出 5w 条数据需要 3分30秒 说明当使用多线程导出时，并发越高，那么单个线程的处理能力会越低
     * 所以大数据量的导出 不推荐使用模版导出  太耗时了
     * 并发数为10 导出 1w条数据一张表的情况  耗时 15秒  官方推荐不要超过 5000 条数据时使用模版导出
     * 上面有不使用模版导出的方式  百万数据导出只需要 15秒左右
     *
     * @return ·
     */
    @Override
    public ResultResponse<String> concurrentTemplateExportAnother() {
        // submit 提交一个有返回值的任务 你可以通过 future.get() 获取任务的结果。如果任务抛出异常，get() 会抛出 ExecutionException。
        for (int i = 0; i < 50; i++) {
            threadPoolTaskExecutor.submit(this::templateExport);
        }
        // execute 提交一个没有返回值的任务
        for (int i = 0; i < 5; i++) {
            threadPoolTaskExecutor.execute(this::templateExport);
        }
        return ResultResponse.success();
    }

    /**
     * 通过线程池的方式 指定 最大队列数 设定拒绝策略为 拒绝时抛出异常， 这样当并发很高 高到队列都装不下时 会返回给前端系统忙 而不至于 疯狂导出撑爆内存
     *
     * @return ·
     */
    @Override
    public ResultResponse<String> actualCombatExport() {
        // 这里我们使用线程池 我们将队列大小设置10 任务满了直接抛出异常告诉前端
        // 这里线程池核心数给了 3 最大线程数给了5 队列给了10  （这个具体是多少 按自己实际情况来）
        try {
            threadPoolTaskExecutor.submit(() -> {
                String fileName = exportPath + UUID.randomUUID() + XLSX;
                EasyExcelFactory.write(fileName, SimpleExportUser.class)
                        .excelType(ExcelTypeEnum.XLSX)
                        .sheet(0, "sheetName")
                        .doWrite(createUsers(SimpleExportUser.class, 1000000));
            });
        } catch (Exception e) {
            e.printStackTrace();
            log.info("系统繁忙，请稍后再试...");
            return ResultResponse.error("系统繁忙，请稍后再试...");
        }
        return ResultResponse.success();
    }

    @Override
    public ResultResponse<String> writeByCustomerStyle() {
        String fileName = exportPath + UUID.randomUUID() + XLSX;
        EasyExcelFactory
                .write(fileName, StudentStyleWrite.class)
                .sheet()
                .doWrite(getStudentStyleWrites());
        return ResultResponse.success();
    }

    private List<StudentStyleWrite> getStudentStyleWrites() {
        ArrayList<StudentStyleWrite> list = ListUtils.newArrayList();
        for (int i = 0; i < 10; i++) {
            list.add(new StudentStyleWrite((long) i, "name" + i, String.valueOf(i), LocalDateTime.now(), i + "年纪" + i + "班", new BigDecimal(i)));
        }
        return list;
    }

    /**
     * 创建数据
     *
     * @return ·
     */
    private <T> List<T> createUsers(Class<T> clazz, Integer userTotal) {
        ArrayList<T> list = new ArrayList<>();
        for (int i = 0; i < userTotal; i++) {
            try {
                Constructor<T> constructor = clazz.getConstructor();
                T user = constructor.newInstance();
                Field[] declaredFields = clazz.getDeclaredFields();
                for (Field declaredField : declaredFields) {
                    declaredField.setAccessible(true);
                    if (Objects.equals(declaredField.getName(), "username")) {
                        declaredField.set(user, "yang" + i);
                    }
                    if (Objects.equals(declaredField.getName(), "password")) {
                        declaredField.set(user, "123456");
                    }
                    if (Objects.equals(declaredField.getName(), "address")) {
                        declaredField.set(user, "成都");
                    }
                    if (Objects.equals(declaredField.getName(), "sex")) {
                        if (i % 2 == 0) {
                            declaredField.set(user, "男");
                        } else declaredField.set(user, "女");
                    }
                    if (Objects.equals(declaredField.getName(), "birthday")) {
                        declaredField.set(user, LocalDateTime.now());
                    }
                    if (Objects.equals(declaredField.getName(), "age")) {
                        declaredField.set(user, (i % 99) + 1);
                    }
                    if (Objects.equals(declaredField.getName(), "height")) {
                        declaredField.set(user, 1.78);
                    }
                    if (Objects.equals(declaredField.getName(), "weight")) {
                        declaredField.set(user, 70.1F);
                    }
                    if (Objects.equals(declaredField.getName(), "heightPercent")) {
                        declaredField.set(user, 1.78);
                    }
                    if (Objects.equals(declaredField.getName(), "weightPercent")) {
                        declaredField.set(user, 70.1F);
                    }


                }
                list.add(user);
            } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        // log.info("导出数据总数： {}", userTotal);
        return list;
    }
}

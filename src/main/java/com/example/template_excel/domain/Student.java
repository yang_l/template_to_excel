package com.example.template_excel.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @Author YangLi
 * @Date 2025/1/7 17:08
 * @注释
 */
@Data
@TableName("excel_student")
public class Student {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String name;

    private Integer sex;

    private LocalDateTime birthday;

    private String clazz;

    private BigDecimal score;
}

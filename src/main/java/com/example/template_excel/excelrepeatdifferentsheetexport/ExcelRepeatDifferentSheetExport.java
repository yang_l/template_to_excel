package com.example.template_excel.excelrepeatdifferentsheetexport;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @Author YangLi
 * @Date 2024/12/27 9:05
 * @注释
 */
@Data
public class ExcelRepeatDifferentSheetExport {

    /**
     * @ExcelProperty  这个注解用来指定标题  这里值为 “用户名” 那么生成的 excel 标题就会有 用户名
     */
    @ExcelProperty(value = "用户名")
    private String username;

    @ExcelProperty(value = "密码")
    private String password;

    @ExcelProperty(value = "地址")
    private String address;

    @ExcelProperty(value = "性别")
    private String sex;
}

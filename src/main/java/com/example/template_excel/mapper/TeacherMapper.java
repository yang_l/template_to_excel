package com.example.template_excel.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.template_excel.domain.Teacher;
import org.apache.ibatis.annotations.Mapper;


import java.util.List;

/**
 * @Author YangLi
 * @Date 2025/1/13 9:30
 * @注释
 */
@Mapper
public interface TeacherMapper extends BaseMapper<Teacher> {

    int batchInsert(List<Teacher> list);
}

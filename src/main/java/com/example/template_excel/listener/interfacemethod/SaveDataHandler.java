package com.example.template_excel.listener.interfacemethod;

import java.util.List;

/**
 * @Author YangLi
 * @Date 2025/1/8 11:38
 * @注释
 */
public interface SaveDataHandler<K, V> {

    void  saveData(List<V> afterConvertList);

    List<V> convertClass(List<K> list, int batchCount);
}

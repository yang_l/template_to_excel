package com.example.template_excel.listener.interfacemethod;

import com.alibaba.excel.util.ListUtils;
import com.example.template_excel.domain.Student;
import com.example.template_excel.dto.StudentDTO;
import com.example.template_excel.mapper.StudentMapper;
import com.example.template_excel.mapstruct.MapStructConvert;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author YangLi
 * @Date 2025/1/8 14:08
 * @注释
 */
@Component
public class StudentDTOHandler implements SaveDataHandler<StudentDTO, Student>{

    @Resource
    private StudentMapper studentMapper;

    @Override
    public void saveData(List<Student> afterConvertList) {
        studentMapper.batchInsert(afterConvertList);
    }

    @Override
    public List<Student> convertClass(List<StudentDTO> list, int batchCount) {
        ArrayList<Student> studentList = ListUtils.newArrayListWithCapacity(batchCount);
        list.forEach(excelUserDTO -> {
            MapStructConvert instance = MapStructConvert.INSTANCE;
            studentList.add(instance.studentDTO2Student(excelUserDTO));
        });
        return studentList;
    }
}

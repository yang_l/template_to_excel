package com.example.template_excel.exceltemplateexport;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Author YangLi
 * @Date 2024/12/27 11:17
 * @注释
 */
@Data
public class ExportTimeAndTotal {

    public ExportTimeAndTotal(LocalDateTime exportTime, Long total) {
        this.exportTime = exportTime;
        this.total = total;
    }

    private LocalDateTime exportTime;

    private Long total;
}

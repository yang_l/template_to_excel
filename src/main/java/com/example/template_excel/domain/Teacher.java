package com.example.template_excel.domain;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.NumberFormat;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.example.template_excel.converter.CustomConverter;
import com.example.template_excel.converter.SexConverter;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Author YangLi
 * @Date 2025/1/10 18:26
 * @注释
 */
@Data
public class Teacher {

    @ExcelIgnore
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * CustomConverter 这里通过这个类  自定义处理 name 我在所有的 name 前面都加上清华大学
     */
    @ExcelProperty(value = "姓名", converter = CustomConverter.class)
    private String name;

    /**
     * SexConverter 这里通过这个类  自定义处理 sex 如果sex 是男 就返回 1 否则 返回 0
     */
    @ExcelProperty(value = "性别", converter = SexConverter.class)
    private String sex;

    @ExcelProperty("生日")
    private LocalDateTime birthday;

    @ExcelProperty(value = "班级")
    private String clazz;

    @NumberFormat("#.00")
    @ExcelProperty("评分")
    private Double score;

}

package com.example.template_excel.domain;

import lombok.Data;

/**
 * @Author YangLi
 * @Date 2023/12/12 15:29
 * @注释
 */
@Data
public class User {

    public User(String username, String password, String address, String sex) {
        this.username = username;
        this.password = password;
        this.address = address;
        this.sex = sex;
    }

    private String username;

    private String password;

    private String address;

    private String sex;
}

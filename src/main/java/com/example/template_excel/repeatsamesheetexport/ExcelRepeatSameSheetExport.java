package com.example.template_excel.repeatsamesheetexport;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @Author YangLi
 * @Date 2024/12/26 18:05
 * @注释
 */
@Data
public class ExcelRepeatSameSheetExport {

    /**
     * @ExcelProperty  这个注解用来指定标题  这里值为 “用户名” 那么生成的 excel 标题就会有 用户名
     */
    @ExcelProperty(value = {"用户", "用户名"})
    private String username;

    @ExcelProperty(value = {"用户", "密码"})
    private String password;

    /**
     * @ExcelIgnore 这个注解用来表示忽略某个字段，这里在 address 这个字段加上这个字段  那么标题和数据都不会有 address
     */
    @ExcelIgnore
    @ExcelProperty(value = {"用户", "地址"})
    private String address;

    @ExcelProperty(value = {"用户", "性别"} ,index = 2)
    private String sex;
}

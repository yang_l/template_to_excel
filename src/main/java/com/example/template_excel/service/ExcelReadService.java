package com.example.template_excel.service;

import com.example.template_excel.response.ResultResponse;

/**
 * @Author YangLi
 * @Date 2025/1/6 11:05
 * @注释
 */
public interface ExcelReadService {

    ResultResponse<String> simpleRead();

    ResultResponse<String> indexOrNameRead();

    ResultResponse<String> repeatedRead();

    ResultResponse<String> converterRead();

    ResultResponse<String> headerRead();

}

package com.example.template_excel.mapstruct;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.example.template_excel.domain.ExcelUser;
import com.example.template_excel.domain.Student;
import com.example.template_excel.dto.ExcelUserDTO;
import com.example.template_excel.dto.StudentDTO;
import com.example.template_excel.dto.StudentHeaderReadDTO;
import com.example.template_excel.dto.StudentIndexOrNameReadDTO;
import com.example.template_excel.enums.SexEnum;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Objects;

/**
 * @Author YangLi
 * @Date 2025/1/10 11:20
 * @注释
 */
@Mapper
public interface MapStructConvert {

    MapStructConvert INSTANCE = Mappers.getMapper(MapStructConvert.class);

    @Mapping(target = "sex", source = "sex", qualifiedByName = "sexString2int")
    ExcelUser excelUserDTO2ExcelUser(ExcelUserDTO excelUserDTO);

    @Mapping(target = "sex", source = "sex", qualifiedByName = "sexString2int")
    @Mapping(target = "score", source = "score", qualifiedByName = "formatScore")
    Student studentDTO2Student(StudentDTO studentDTO);

    @Mapping(target = "sex", source = "sex", qualifiedByName = "sexString2int")
    Student studentIndexOrNameReadDTO2Student(StudentIndexOrNameReadDTO studentIndexOrNameReadDTO);

    @Mapping(target = "sex", source = "sex", qualifiedByName = "sexString2int")
    @Mapping(target = "score", source = "score", qualifiedByName = "formatScore")
    Student studentHeaderReadDTO2Student(StudentHeaderReadDTO studentHeaderReadDTO);

    @Named("sexString2int")
    default Integer stringSex2int(String oldValue) {
        if (StringUtils.isNotBlank(oldValue)) {
            switch (oldValue) {
                case "0":
                    return 0;
                case "1":
                    return 1;
                case "2":
                    return 2;
                default:
                    for (SexEnum value : SexEnum.values()) {
                        if (Objects.equals(value.getSex(), oldValue)) {
                            return value.getCode();
                        }
                    }
            }
        }
        return SexEnum.OTHER.getCode();
    }

    @Named("formatScore")
    default BigDecimal formatScore(BigDecimal score) {
        if (score != null) {
            // 使用 DecimalFormat 格式化为两位小数
            DecimalFormat df = new DecimalFormat("#.00");
            return new BigDecimal(df.format(score));
        }
        return null;
    }
}

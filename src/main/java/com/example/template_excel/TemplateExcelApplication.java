package com.example.template_excel;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.example.template_excel.mapper")
@SpringBootApplication
public class TemplateExcelApplication {

    public static void main(String[] args) {
        SpringApplication.run(TemplateExcelApplication.class, args);
    }

}

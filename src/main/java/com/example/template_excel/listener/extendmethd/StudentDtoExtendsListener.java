package com.example.template_excel.listener.extendmethd;

import com.example.template_excel.domain.Student;
import com.example.template_excel.mapper.StudentMapper;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @Author YangLi
 * @Date 2025/1/7 14:09
 * @注释
 */
@EqualsAndHashCode(callSuper = false)
public abstract class StudentDtoExtendsListener<K> extends ExcelReadExtendsListener<K, Student> {
                                                                        /**
                                                                         * 这里K还是由调用方传递 V 写死 表示这个类只处理 Student 入库
                                                                         * 父类的 convertClass 由调用方实现 我不关心你怎么进行类型转换
                                                                         * 反正最终 我只插入父类 afterConvertList 中的数据
                                                                         */

    private final StudentMapper studentMapper;

    protected StudentDtoExtendsListener(int batchCount, StudentMapper studentMapper) {
        super(batchCount);
        this.studentMapper = studentMapper;
    }

    @Override
    protected void saveData(List<Student> afterConvertList) {
        studentMapper.batchInsert(afterConvertList);
    }

    // 父类中 convertClass 抽象方法 我没实现 由子类继续实现
}

package com.example.template_excel.enums;

/**
 * @Author YangLi
 * @Date 2025/1/6 14:41
 * @注释
 */
public enum SexEnum {

    MALE(0, "女"),
    FEMALE(1, "男"),
    OTHER(2, "不详");

    private final Integer code;

    private final String sex;

    SexEnum(Integer code, String sex) {
        this.code = code;
        this.sex = sex;
    }

    public Integer getCode() {
        return code;
    }

    public String getSex() {
        return sex;
    }
}

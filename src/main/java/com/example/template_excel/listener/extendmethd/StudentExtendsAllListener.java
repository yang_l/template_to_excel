package com.example.template_excel.listener.extendmethd;

import com.alibaba.excel.util.ListUtils;
import com.example.template_excel.domain.Student;
import com.example.template_excel.dto.StudentDTO;
import com.example.template_excel.mapper.StudentMapper;
import com.example.template_excel.mapstruct.MapStructConvert;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author YangLi
 * @Date 2025/1/9 17:01
 * @注释
 */
@EqualsAndHashCode(callSuper = false)
public class StudentExtendsAllListener extends ExcelReadExtendsListener<StudentDTO, Student> {

    private final StudentMapper studentMapper;

    public StudentExtendsAllListener(StudentMapper studentMapper) {
        this.studentMapper = studentMapper;
    }

    public StudentExtendsAllListener(int batchCount, StudentMapper studentMapper) {
        super(batchCount);
        this.studentMapper = studentMapper;
    }

    @Override
    protected void saveData(List<Student> afterConvertList) {
        studentMapper.batchInsert(afterConvertList);
    }

    @Override
    protected List<Student> convertClass(List<StudentDTO> list, int batchCount) {
        ArrayList<Student> studentList = ListUtils.newArrayListWithExpectedSize(batchCount);
        list.forEach(item -> {
            MapStructConvert instance = MapStructConvert.INSTANCE;
            studentList.add(instance.studentDTO2Student(item));
        });
        return studentList;
    }
}

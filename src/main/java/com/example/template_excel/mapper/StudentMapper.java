package com.example.template_excel.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.template_excel.domain.Student;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author YangLi
 * @Date 2025/1/7 17:14
 * @注释
 */
@Mapper
public interface StudentMapper extends BaseMapper<Student> {

    void batchInsert(List<Student> list);
}

package com.example.template_excel.dto;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.NumberFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @Author YangLi
 * @Date 2025/1/13 11:05
 * @注释
 */
@Data
public class StudentHeaderReadDTO {

    @ExcelIgnore
    private Long id;

    /**
     * 通过 {"","",""...} 这种方式去构建头部
     */
    @ExcelProperty(value = {"学生表", "姓名"})
    private String name;

    @ExcelProperty(value = {"学生表", "性别"})
    private String sex;

    @ExcelProperty({"学生表", "生日"})
    private LocalDateTime birthday;

    @ExcelProperty(value = {"学生表", "班级"})
    private String clazz;

    @NumberFormat("#.##")
    @ExcelProperty(value = {"学生表", "成绩"})
    private BigDecimal score;
}

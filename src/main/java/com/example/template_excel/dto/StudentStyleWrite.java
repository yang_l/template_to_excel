package com.example.template_excel.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.NumberFormat;
import com.alibaba.excel.annotation.write.style.*;
import com.alibaba.excel.enums.poi.FillPatternTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @Author YangLi
 * @Date 2025/1/13 15:32
 * @注释
 */
@Data
@AllArgsConstructor
// 头背景设置成红色 IndexedColors.RED.getIndex()
@HeadStyle(fillPatternType = FillPatternTypeEnum.SOLID_FOREGROUND, fillForegroundColor = 10)
// 头字体设置成20
@HeadFontStyle(fontHeightInPoints = 20)
// 内容的背景设置成绿色 IndexedColors.GREEN.getIndex()
@ContentStyle(fillPatternType = FillPatternTypeEnum.SOLID_FOREGROUND, fillForegroundColor = 17)
// 内容字体设置成20
@ContentFontStyle(fontHeightInPoints = 20)
// 头部行高
@HeadRowHeight(20)
// 内容行高
@ContentRowHeight(10)
// 列宽
@ColumnWidth(25)
public class StudentStyleWrite {

    /* @HeadStyle @HeadFontStyle @ContentStyle @ContentFontStyle
    * 放在类上是对整个头部应用
    * 放在字段上表示 对头部中的某个格子应用
    * 如果都使用 以字段上的为准
    *
    * 下面举例  没有覆盖类上的注解的字段 使用的颜色是 类上字段指定的颜色和字体风格
    * 覆盖了了类上注解的字段 使用的是字段上的注解使用的颜色和字体风格
    */

    /**
     * // 头部行高
     * @HeadRowHeight(20) `
     * // 内容行高
     * @ContentRowHeight(10) `
     * // 列宽
     * @ColumnWidth(25) `
     *
     * 这几个注解也是一样的  在类上就全使用，在字段上使用就单独生效
     */

    // 字符串的头背景设置成粉红 IndexedColors.PINK.getIndex()
    @HeadStyle(fillPatternType = FillPatternTypeEnum.SOLID_FOREGROUND, fillForegroundColor = 14)
    // 字符串的头字体设置成30
    @HeadFontStyle(fontHeightInPoints = 30)
    // 字符串的内容的背景设置成天蓝 IndexedColors.SKY_BLUE.getIndex()
    @ContentStyle(fillPatternType = FillPatternTypeEnum.SOLID_FOREGROUND, fillForegroundColor = 40)
    // 字符串的内容字体设置成20
    @ContentFontStyle(fontHeightInPoints = 20)
    @ExcelProperty(value = "编号")
    private Long id;

    @HeadStyle(fillPatternType = FillPatternTypeEnum.SOLID_FOREGROUND, fillForegroundColor = 14)
    @HeadFontStyle(fontHeightInPoints = 30)
    @ContentStyle(fillPatternType = FillPatternTypeEnum.SOLID_FOREGROUND, fillForegroundColor = 40)
    @ContentFontStyle(fontHeightInPoints = 20)
    @ExcelProperty(value = "姓名")
    private String name;

    @HeadFontStyle(fontHeightInPoints = 30)
    @ContentFontStyle(fontHeightInPoints = 20)
    @ExcelProperty(value = "性别")
    private String sex;

    @HeadFontStyle(fontHeightInPoints = 30)
    @ContentFontStyle(fontHeightInPoints = 20)
    @ExcelProperty("生日")
    private LocalDateTime birthday;

    @HeadFontStyle(fontHeightInPoints = 30)
    @ContentFontStyle(fontHeightInPoints = 20)
    @ExcelProperty(value = "班级")
    private String clazz;

    @HeadFontStyle(fontHeightInPoints = 30)
    @ContentFontStyle(fontHeightInPoints = 20)
    @NumberFormat("#.##")
    @ExcelProperty(value = "成绩")
    private BigDecimal score;
}

package com.example.template_excel.response;

import com.alibaba.fastjson2.JSON;
import lombok.Data;

/**
 * @description: 自定义数据传输
 * @author: YangLi
 * @date: 2023/7/20
 */
@Data
public class ResultResponse<T> {
    /**
     * 响应代码
     */
    private String code;

    /**
     * 响应消息
     */
    private String message;

    /**
     * 请求地址
     */
    private String requestURI;

    /**
     * 响应结果
     */
    private T resultData;

    private Boolean success;


    public ResultResponse() {
    }

    public ResultResponse(BaseErrorInfoInterface errorInfo) {
        this.code = errorInfo.getResultCode();
        this.message = errorInfo.getResultMsg();
        this.success = false;
    }

    /**
     * 成功
     *
     * @return .
     */
    public static <T> ResultResponse<T> success() {
        ResultResponse<T> rb = new ResultResponse<>();
        rb.setCode(ExceptionEnum.SUCCESS.getResultCode());
        rb.setMessage(ExceptionEnum.SUCCESS.getResultMsg());
        rb.setSuccess(true);
        return rb;
    }

    /**
     * 成功
     *
     * @return .
     */
    public static <T> ResultResponse<T> success(BaseErrorInfoInterface errorInfo) {
        ResultResponse<T> rb = new ResultResponse<>();
        rb.setCode(errorInfo.getResultCode());
        rb.setMessage(errorInfo.getResultMsg());
        rb.setSuccess(true);
        return rb;
    }

    /**
     * 成功
     *
     * @param data .
     * @return .
     */
    public static <T> ResultResponse<T> success(T data) {
        ResultResponse<T> rb = new ResultResponse<>();
        rb.setCode(ExceptionEnum.SUCCESS.getResultCode());
        rb.setMessage(ExceptionEnum.SUCCESS.getResultMsg());
        rb.setResultData(data);
        rb.setSuccess(true);
        return rb;
    }

    /**
     * 失败
     */
    public static <T> ResultResponse<T> error(BaseErrorInfoInterface errorInfo, String requestURI) {
        ResultResponse<T> rb = new ResultResponse<>();
        rb.setCode(errorInfo.getResultCode());
        rb.setMessage(errorInfo.getResultMsg());
        rb.setRequestURI(requestURI);
        rb.setSuccess(false);
        rb.setResultData(null);
        return rb;
    }

    /**
     * 失败
     */
    public static <T> ResultResponse<T> error(String code, String message, String requestURI) {
        ResultResponse<T> rb = new ResultResponse<>();
        rb.setCode(code);
        rb.setMessage(message);
        rb.setRequestURI(requestURI);
        rb.setSuccess(false);
        rb.setResultData(null);
        return rb;
    }

    /**
     * 失败
     */
    public static <T> ResultResponse<T> error(ExceptionEnum exceptionEnum, String requestURI) {
        ResultResponse<T> rb = new ResultResponse<>();
        rb.setCode(exceptionEnum.getResultCode());
        rb.setMessage(exceptionEnum.getResultMsg());
        rb.setRequestURI(requestURI);
        rb.setResultData(null);
        rb.setSuccess(false);
        return rb;
    }


    /**
     * 失败
     */
    public static <T> ResultResponse<T> error(String code, String message, T result, String requestURI) {
        ResultResponse<T> rb = new ResultResponse<>();
        rb.setCode(code);
        rb.setMessage(message);
        rb.setRequestURI(requestURI);
        rb.setResultData(result);
        rb.setSuccess(false);
        return rb;
    }

    /**
     * 失败
     */
    public static <T> ResultResponse<T> error(ExceptionEnum exceptionEnum, T result, String requestURI) {
        ResultResponse<T> rb = new ResultResponse<>();
        rb.setCode(exceptionEnum.getResultCode());
        rb.setMessage(exceptionEnum.getResultMsg());
        rb.setRequestURI(requestURI);
        rb.setResultData(result);
        rb.setSuccess(false);
        return rb;
    }

    /**
     * 失败
     */
    public static <T> ResultResponse<T> error(ExceptionEnum exceptionEnum) {
        ResultResponse<T> rb = new ResultResponse<>();
        rb.setCode(exceptionEnum.getResultCode());
        rb.setMessage(exceptionEnum.getResultMsg());
        rb.setSuccess(false);
        return rb;
    }

    /**
     * 失败
     */
    public static <T> ResultResponse<T> error(String message, String requestURI) {
        ResultResponse<T> rb = new ResultResponse<>();
        rb.setCode("500");
        rb.setMessage(message);
        rb.setRequestURI(requestURI);
        rb.setSuccess(false);
        return rb;
    }

    public static <T> ResultResponse<T> error(String message) {
        ResultResponse<T> rb = new ResultResponse<>();
        rb.setCode("500");
        rb.setMessage(message);
        rb.setSuccess(false);
        return rb;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }

}
package com.example.template_excel.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;


/**
 * @Author YangLi
 * @Date 2024/12/26 14:40
 * @注释
 */
@Slf4j
@Aspect
@Component
public class TimeConsumingAspect {

    @Pointcut(value = "@annotation(com.example.template_excel.annotation.TimeConsuming)")
    private void timeConsuming(){
        // 切点
    }

    @Around("timeConsuming()")
    private Object aroundMethod(ProceedingJoinPoint joinPoint) throws Throwable {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        long startTime = System.currentTimeMillis();
        Object proceed = joinPoint.proceed();
        log.info(method.getName() + "接口耗时： {}ms", System.currentTimeMillis() - startTime);
        return proceed;
    }
}

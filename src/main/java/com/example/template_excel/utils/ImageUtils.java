package com.example.template_excel.utils;

import com.alibaba.excel.metadata.data.ImageData;
import com.alibaba.excel.metadata.data.WriteCellData;


import java.util.ArrayList;
import java.util.List;

/**
 * @Author YangLi
 * @Date 2023/12/12 14:28
 * @注释
 */
public class ImageUtils {

    private ImageUtils() {
        // 私有化
    }

    public static WriteCellData<Void> imageCells(byte[] bytes) {


        WriteCellData<Void> writeCellData = new WriteCellData<>();
        // 这里可以设置为 EMPTY 则代表不需要其他数据了
        //writeCellData.setType(CellDataTypeEnum.EMPTY);

        // 可以放入多个图片
        List<ImageData> imageDataList = new ArrayList<>();
        writeCellData.setImageDataList(imageDataList);


        ImageData imageData = new ImageData();
        imageDataList.add(imageData);
        // 设置图片
        imageData.setImage(bytes);
        // 图片类型
        //imageData.setImageType(ImageData.ImageType.PICTURE_TYPE_PNG);
        // 上 右 下 左 需要留空，这个类似于 css 的 margin；这里实测 不能设置太大 超过单元格原始大小后 打开会提示修复。暂时未找到很好的解法。
        imageData.setTop(10);
        imageData.setRight(10);
        imageData.setBottom(10);
        imageData.setLeft(10);

        // * 设置图片的位置。Relative表示相对于当前的单元格index。first是左上点，last是对角线的右下点，这样确定一个图片的位置和大小。
        // 第1张图片相对位置
        // 图片从第几行开始
        imageData.setRelativeFirstRowIndex(0);
        // 图片从第几列开始
        imageData.setRelativeFirstColumnIndex(0);
        // 图片需要占用多少行 需要从0开始算  比如占用6行  就填5
        imageData.setRelativeLastRowIndex(14);
        // 图片需要占用多少列 需要从0开始算  比如占用6列  就填5
        imageData.setRelativeLastColumnIndex(5);


        return writeCellData;
    }

}

package com.example.template_excel.domain;

import com.alibaba.excel.metadata.data.WriteCellData;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @Author YangLi
 * @Date 2023/12/11 14:29
 * @注释
 */
@Data
@AllArgsConstructor
public class ExcelInfo {

    private String procuratorate;

    private String excelName;

    private String fileName;

    private String createTime;

    private String sheetName;

    private WriteCellData<Void> img;
}

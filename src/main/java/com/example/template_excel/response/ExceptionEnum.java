package com.example.template_excel.response;

/**
 * @description: 异常处理枚举类
 * @author: YangLi
 * @date: 2023/7/20
 */
public enum ExceptionEnum implements BaseErrorInfoInterface {

    // 数据操作错误定义
    SUCCESS("200", "成功!"),
    BODY_NOT_MATCH("400", "请求的数据格式不符!"),
    USERNAME_EXIST("4001", "用户名已存在!"),
    NULL_POINTER_EXCEPTION("4002", "空指针异常"),
    ACCOUNT_CREATE_FAILURE("4004", "用户创建失败"),
    ACCOUNT_CREATE_SUCCESS("4005", "用户创建成功"),
    LOGIN_FIRST("4006", "请先登录"),
    USER_NOT_EXIST("4006", "用户不存在"),
    SIGNATURE_NOT_MATCH("401", "请求的数字签名不匹配!"),
    NOT_FOUND("404", "未找到该资源!"),
    PARAMS_NOT_CONVERT("4003", "类型转换不对!"),
    ILLEGAL_STATE("403", "参数不合法!"),
    INTERNAL_SERVER_ERROR("500", "服务器内部错误!"),
    FLOW_EXCEPTION("5001", "接口限流"),
    DEGRADE_EXCEPTION("5002", "服务降级了"),
    PARAM_FLOW_EXCEPTION("5003", "热点参数限流"),
    SYSTEM_BLOCK_EXCEPTION("5004", "系统保护"),
    AUTHORITY_EXCEPTION("5005", "没有访问权限"),
    ARITHMETIC_EXCEPTION("4008", "算数异常"),
    MY_EXCEPTION("4009", "自定义异常"),
    UNSUPPORTED_MEDIA_TYPE("415", "不支持的媒体类型"),
    CONNECTION_REFUSED("5006", "连接被拒绝(redis,socket,mysql...)"),
    METHOD_NOT_ALLOWED("405", "请求方式不正确"),
    CAPTCHA_NOT_EXIST("504", "请输入验证码！"),
    CAPTCHA_EXPIRED("504", "验证码已过期！"),
    CAPTCHA_ERROR("504", "验证码错误！"),
    SEND_SMS_ERROR("505", "验证码发送失败！"),
    SEND_SMS_SUCCESS("506", "短信发送成功！"),
    SEND_EMAIL_SUCCESS("507", "邮件发送成功！"),
    SEND_EMAIL_ERROR("508", "邮件发送失败！"),
    DIFFERENT_PASSWORD("509", "两次输入密码不一致！"),
    SERVER_BUSY("503", "服务器正忙，请稍后再试!");
    /**
     * 错误码
     */
    private final String resultCode;

    /**
     * 错误描述
     */
    private final String resultMsg;

    // 之所以要写这个样构造方法 使我们可以在enum类中 列举自己想要的枚举 如果去掉 上面的枚举就不可以存在
    // 枚举的构造方法是私有的 只能在内部使用 枚举类的构造方法即使没有访问修饰符 它依旧是私有的 如果申明为provider或public 就会出现编译错误
    // 所以这使得enum即使没有显式声明构造函数也不可实例化。
    ExceptionEnum(String resultCode, String resultMsg) {
        this.resultCode = resultCode;
        this.resultMsg = resultMsg;
    }

    @Override
    public String getResultCode() {
        return resultCode;
    }

    @Override
    public String getResultMsg() {
        return resultMsg;
    }
}

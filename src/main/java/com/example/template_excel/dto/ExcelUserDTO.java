package com.example.template_excel.dto;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.excel.annotation.format.NumberFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Author YangLi
 * @Date 2025/1/7 9:47
 * @注释
 */
@Data
public class ExcelUserDTO {

    @ExcelIgnore
    private Long id;

    /**
     * @ExcelProperty  这个注解用来指定标题  这里值为 “用户名” 那么生成的 excel 标题就会有 用户名
     */
    @ExcelProperty(value = "用户名")
    private String username;

    @ExcelProperty(value = "密码")
    private String password;

    @ExcelProperty(value = "地址")
    private String address;

    @ExcelProperty(value = "性别")
    private String sex;

    /**
     * 写到excel 用年月日的格式
     */
    @DateTimeFormat("yyyy年MM月dd日HH时mm分ss秒")
    @ExcelProperty(value = "生日")
    private LocalDateTime birthday;

    /**
     * Integer 类型
     */
    @NumberFormat("#")
    @ExcelProperty(value = "年龄")
    private Integer age;

    /**
     * Double 类型
     */
    @NumberFormat("#.##")
    @ExcelProperty(value = "身高")
    private Double height;

    /**
     *  类型 Float
     */
    @NumberFormat("#.#")
    @ExcelProperty(value = "体重")
    private Float weight;

    /**
     * Double 类型
     */
    @NumberFormat("#.#%")
    @ExcelProperty(value = "身高百分比")
    private Double heightPercent;

    /**
     *  类型 Float
     */
    @NumberFormat("#.##%")
    @ExcelProperty(value = "体重百分比")
    private Float weightPercent;
}

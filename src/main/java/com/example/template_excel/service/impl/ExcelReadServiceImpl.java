package com.example.template_excel.service.impl;

import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.read.listener.PageReadListener;
import com.alibaba.excel.read.listener.ReadListener;
import com.alibaba.excel.read.metadata.ReadSheet;
import com.alibaba.excel.util.ListUtils;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.example.template_excel.domain.Student;
import com.example.template_excel.domain.Teacher;
import com.example.template_excel.dto.ExcelUserDTO;
import com.example.template_excel.domain.ExcelUser;
import com.example.template_excel.dto.StudentDTO;
import com.example.template_excel.dto.StudentHeaderReadDTO;
import com.example.template_excel.dto.StudentIndexOrNameReadDTO;
import com.example.template_excel.listener.extendmethd.*;
import com.example.template_excel.listener.interfacemethod.ExcelInterfaceReadListener;
import com.example.template_excel.listener.interfacemethod.ExcelUserDTOHandler;
import com.example.template_excel.listener.interfacemethod.SaveDataHandler;
import com.example.template_excel.listener.interfacemethod.StudentDTOHandler;
import com.example.template_excel.mapper.ExcelUserMapper;
import com.example.template_excel.mapper.StudentMapper;
import com.example.template_excel.mapper.TeacherMapper;
import com.example.template_excel.mapstruct.MapStructConvert;
import com.example.template_excel.response.ResultResponse;
import com.example.template_excel.service.ExcelReadService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author YangLi
 * @Date 2025/1/6 11:06
 * @注释
 */
@Slf4j
@Service
public class ExcelReadServiceImpl implements ExcelReadService {

    @Resource
    private TeacherMapper teacherMapper;

    @Resource
    private StudentMapper studentMapper;

    @Resource
    private ExcelUserMapper excelUserMapper;

    @Resource
    private ExcelUserDTOHandler excelUserDTOHandler;

    @Resource
    private StudentDTOHandler studentDTOHandler;

    @Value("${excel.excelUserReadPath}")
    private String excelUserReadPath;

    @Value("${excel.teacherReadPath}")
    private String teacherReadPath;

    @Value("${excel.studentReadPath}")
    private String studentReadPath;

    @Value("${excel.headerReadPath}")
    private String headerReadPath;

    public static final int BATCH_COUNT = 19;

    // 这里直接从本地文件读取
    @Override
    public ResultResponse<String> simpleRead() {

        // 第一种写法直接使用 EasyExcel 自带的 pageListener
        // 这里 list -> {} 表示会生成一个 数量为 batchCount 的list    -> {} 表示你想对这个list进行怎样的操作
        // PageReadListener 的构造方法需要一个 Consumer<List<T>>       Consumer 里面有一个 default 的方法  还有一个 accept() 方法
        // 我们这里 list -> {} 就是表示对 accept 方法的实现
        // 再说简单点 list -> {} 就表示是我们传入的 Consumer  以前我们使用接口的方式来表达实现类  就类似于 第 217 行那个方法  你需要通过接口的实现的方式来写里面的方法
        // 说白了就是 Consumer 接收一个参数 然后对其进行一些操作
        PageReadListener<ExcelUserDTO> pageReadListener = new PageReadListener<>(list -> {
            // 将数据转换为BO
            ArrayList<ExcelUser> boList = new ArrayList<>();
            for (ExcelUserDTO excelUser : list) {
                MapStructConvert instance = MapStructConvert.INSTANCE;
                boList.add(instance.excelUserDTO2ExcelUser(excelUser));
            }
            // 这里分批次入库 比如这里每次读取10  我们就每 10 条数据就插入一次数据库
            excelUserMapper.batchInsert(boList);
        }, BATCH_COUNT);


        EasyExcelFactory.read(excelUserReadPath, ExcelUserDTO.class, pageReadListener)
                // .doRead(); 只读取第一个 sheet 页  如果 .sheet() 没有配置的话   如果你的 .sheet(1) 是这样的  就表示读取第二个 sheet 页
                .sheet()
                .doRead();

        // 写法2 自己搞一个类似 pageListener 的监听器  这里使用的就是匿名内部类
        EasyExcelFactory
                // ReadListener 的匿名实现类
                .read(excelUserReadPath, ExcelUserDTO.class, new ReadListener<ExcelUserDTO>() {

                    /**
                     * 单次读取的数量 int 类型属于常量池里面的东西  所以可以直接使用 static final 修饰 如果是包装类就需要
                     */
                    public static final int BATCH_COUNT = 100;

                    /**
                     * 搞一个初始大小为 10 的 list 来存放我们单次读取的数据
                     */
                    private List<ExcelUserDTO> list = ListUtils.newArrayListWithCapacity(BATCH_COUNT);

                    /**
                     *
                     * @param excelUserDTO 读取 excel 每一行用什么类型来接收
                     * @param analysisContext 上下文信息
                     */
                    @Override
                    public void invoke(ExcelUserDTO excelUserDTO, AnalysisContext analysisContext) {
                        list.add(excelUserDTO);
                        // 每读取一行就判断 list 是否达到了批量插入的数据量 如果是就插入数据库
                        if (list.size() >= BATCH_COUNT) {
                            saveData();
                            // 存储完成清理 list
                            list = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
                        }
                    }

                    /**
                     * excel 读完之后 会调用这个方法
                     * @param analysisContext 上下文信息
                     */
                    @Override
                    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                        if (!CollectionUtils.isEmpty(list)) {
                            saveData();
                        }
                    }

                    /**
                     * 加上存储数据库
                     */
                    private void saveData() {
                        ArrayList<ExcelUser> excelUsers = ListUtils.newArrayListWithCapacity(BATCH_COUNT);
                        list.forEach(excelUserDTO -> {
                            MapStructConvert instance = MapStructConvert.INSTANCE;
                            excelUsers.add(instance.excelUserDTO2ExcelUser(excelUserDTO));
                        });
                        excelUserMapper.batchInsert(excelUsers);
                    }
                })
                .sheet()
                // .doRead(); 只读取第一个 sheet 页  如果 .sheet() 没有配置的话   如果你的 .sheet(1) 是这样的  就表示读取第二个 sheet 页
                .doRead();

        // 写法3 将上面的匿名内部类提取出来 写一个 listener 去实现ReadListener,  但是数据插入这个东西没办法统一处理 使用抽象方法让使用方自己实现
        // 我们在里面写一个抽象方法 在new的时候自己去实现数据插入的具体执行 如下
        // // 这里可以看出来 我们将匿名内部类 抽象成了一个抽象类  这样只需要去 实现其抽象方法即可  不像写法2 需要搞一个类  简化了一部分
        EasyExcelFactory
                .read(excelUserReadPath, ExcelUserDTO.class, new ExcelReadExtendsListener<ExcelUserDTO, ExcelUser>(BATCH_COUNT) {
                    @Override
                    protected void saveData(List<ExcelUser> afterConvertList) {
                        excelUserMapper.batchInsert(afterConvertList);
                    }

                    @Override
                    protected List<ExcelUser> convertClass(List<ExcelUserDTO> list, int batchCount) {
                        ArrayList<ExcelUser> excelUserList = ListUtils.newArrayListWithExpectedSize(batchCount);
                        list.forEach(item -> {
                            MapStructConvert instance = MapStructConvert.INSTANCE;
                            excelUserList.add(instance.excelUserDTO2ExcelUser(item));
                        });
                        return excelUserList;
                    }
                })
                .sheet()
                // .doRead(); 只读取第一个 sheet 页  如果 .sheet() 没有配置的话   如果你的 .sheet(1) 是这样的  就表示读取第二个 sheet 页
                .doRead();

        // 写法4 上面3的写法可重用性还是不是很高  比如 我在另外一个地方 也是用该方法 并且他们的泛型是同一个 还是需要重写 saveData的方法
        // 那么我们使用字类去 extents ExcelReadListener 是不是就可以复用了呢 子类将 saveData 方法给实现即可
        // 还是以上面那个插入为例：
        EasyExcelFactory
                .read(excelUserReadPath, ExcelUserDTO.class, new ExcelUserDtoExtendsListener(BATCH_COUNT, excelUserMapper))
                .sheet()
                // .doRead(); 只读取第一个 sheet 页    如果 .sheet() 没有配置的话   如果你的 .sheet(1) 是这样的  就表示读取第二个 sheet 页
                .doRead();

        // 我们再写一个读取学生excel表的例子  这样是不是就很方便了
        EasyExcelFactory
                .read(studentReadPath, StudentDTO.class, new StudentDtoExtendsListener<StudentDTO>(BATCH_COUNT, studentMapper) {
                    @Override
                    protected List<Student> convertClass(List<StudentDTO> list, int batchCount) {
                        ArrayList<Student> studentList = ListUtils.newArrayListWithExpectedSize(batchCount);
                        list.forEach(studentDTO -> {
                            MapStructConvert instance = MapStructConvert.INSTANCE;
                            studentList.add(instance.studentDTO2Student(studentDTO));
                        });
                        return studentList;
                    }
                })
                // .doReadAll() 表示读取所有的 sheet 页  如果想读多有的 sheet 页 就不要在调用 .sheet()  .sheet() 用来配置 读取哪一个 sheet 页的
                .doReadAll();

        // 写法5 除了使用 抽象子类 还可以使用接口来来实现 excelUserDTOHandler
        // 可以看见使用 接口和抽象的方式其实差别不大  一个使用过子类来写父类的 saveData 抽象方法
        // 一个直接定义一个接口的属性 -> private final SaveDataHandler<T> saveDataHandler;
        // 通过构造方法传入接口的实现类 让实现类去实现 接口中的 saveData 方法
        // 接口实现 和继承实现 一个需要维护多个实现类 一个需要维护多个子类
        EasyExcelFactory
                .read(excelUserReadPath, ExcelUserDTO.class, new ExcelInterfaceReadListener<>(excelUserDTOHandler))
                // .doReadAll() 表示读取所有的 sheet 页  如果想读多有的 sheet 页 就不要在调用 .sheet()  .sheet() 用来配置 读取哪一个 sheet 页的
                .doReadAll();

        EasyExcelFactory
                .read(studentReadPath, StudentDTO.class, new ExcelInterfaceReadListener<>(BATCH_COUNT, studentDTOHandler))
                // .doReadAll() 表示读取所有的 sheet 页  如果想读多有的 sheet 页 就不要在调用 .sheet()  .sheet() 用来配置 读取哪一个 sheet 页的
                .doReadAll();

        // 写法 5 是将处理器直接交给 spring 管理然后注入
        // 写法 6 这里是直接new 一个处理器接口 然后实现接口中的方法，使用匿名实现类来实现接口中的两个方法  其实效果都是一样的 只是写法不同
        EasyExcelFactory
                .read(studentReadPath, StudentDTO.class, new ExcelInterfaceReadListener<>(BATCH_COUNT, new SaveDataHandler<StudentDTO, Student>() {
                    @Override
                    public void saveData(List<Student> afterConvertList) {
                        studentMapper.batchInsert(afterConvertList);
                    }

                    @Override
                    public List<Student> convertClass(List<StudentDTO> list, int batchCount) {
                        ArrayList<Student> studentList = ListUtils.newArrayListWithExpectedSize(batchCount);
                        list.forEach(item -> {
                            MapStructConvert instance = MapStructConvert.INSTANCE;
                            studentList.add(instance.studentDTO2Student(item));
                        });
                        return studentList;
                    }
                }))
                // .doReadAll() 表示读取所有的 sheet 页  如果想读多有的 sheet 页 就不要在调用 .sheet()  .sheet() 用来配置 读取哪一个 sheet 页的
                .doReadAll();
        return ResultResponse.success();
    }

    @Override
    public ResultResponse<String> indexOrNameRead() {
        // 其实这里怎么读 上面 simpleRead() 这个方法中的5种方式都可以
        // 上面第 213 行那个方法  我们使用 StudentDTO.class 是读所有列的
        // 这里 由于我们需要读取指定列的数据 所以需要重新给一个 Class -->>> StudentIndexOrNameReadDTO.class
        EasyExcelFactory
                .read(studentReadPath, StudentIndexOrNameReadDTO.class, getStudentDtoExtendsListener())
                .sheet()
                // 这里调用 .sheet() 但是没有传递 sheetName 或 sheetNo  默认读第 一个sheet
                .doRead();
        return ResultResponse.success();
    }

    @Override
    public ResultResponse<String> repeatedRead() {
        // .doReadAll() 这个方法表示会读全部的 sheet 页
        EasyExcelFactory
                .read(studentReadPath, StudentDTO.class, new StudentExtendsAllListener(BATCH_COUNT, studentMapper))
                .doReadAll();

        // 读取部分 sheet 页  下面就表示要读取 第一个 和第二个 sheet 页
        try (ExcelReader excelReader = EasyExcelFactory.read(studentReadPath).build()) {
            // 构建第一个 ReadSheet
            ReadSheet readSheet1 = EasyExcelFactory
                    // 表示读第一个 sheet 页
                    .readSheet(0)
                    // 表示用哪一个类来接收 excel 中的数据
                    .head(StudentDTO.class)
                    // 注册一个读取数据的监听器 excel 是一行一行的读取的  每一行读取完成后需要干什么 由 StudentExtendsAllListener 决定
                    .registerReadListener(new StudentExtendsAllListener(studentMapper))
                    // 构建 readSheet
                    .build();

            // 构建第二个 ReadSheet
            ReadSheet readSheet2 = EasyExcelFactory
                    // 表示读取 第二个 sheet 页
                    .readSheet(1)
                    // 这里还可以指定用什么类来接收第二页的 数据 很方便
                    .head(StudentIndexOrNameReadDTO.class)
                    .registerReadListener(getStudentDtoExtendsListener())
                    .build();
            // 这里注意 一定要把sheet1 sheet2 一起传进去，不然有个问题就是03版的excel 会读取多次，浪费性能
            excelReader.read(readSheet1, readSheet2);
        }
        return ResultResponse.success();
    }

    /* 上面所有的用到所有的读取 都是通过我自己 的抽象方法或者接口 去实现 Bean 与 Bean 之间的转换
     * 相当于 我们现将 读取到的数据 放到 DTO 中  然后将 DTO 转成 entity 最后入库 我个人还是觉得这种方式更好一些 接收类和数据层分开
     * 其实 easyExcel 还提供了读取的时候直接转换的功能 我们这里举一些简单的例子 主要还是通过传入的 Class 字段上的注解 如果有自己的需求 那么可以实现 ConvertListener 接口
     * * 为了方便理解  我下面整几个例子
     * 由于上面 所有的例子中 都是用的 ExcelReadExtendsListener 的子类  或者 ExcelInterfaceReadListener 的实现类
     * 但是我在上面两个接口中都加入了 Bean 与 Bean 转化的 抽象方法 或 接口 所以 我重新写一个 ReadListener 实现类 以便下面测试
     * 说这个的意思主要就是想表达 用 entity 来装excel中读取的数据  这样就可以直接入库  不用转成 BO
     * 但是接收和入库 涉及参数转化  比如 字段 sex 在 excel 中是 男/女  但是入库 希望是 F/M 或者 0/1 这种 我们在读取的时候直接进行转换
     * 我想表达的意思就是 可以边读取 excel 中的数据 边进行转换 然后放到你想放到的类中
     */

    @Override
    public ResultResponse<String> converterRead() {

        // 是通过 Converter 在读取的时候直接处理字段 具体请进 Teacher 中查看具体转换情况
        EasyExcelFactory.read(teacherReadPath, Teacher.class, new TeacherReadListener<Teacher>() {
            @Override
            protected void saveData(List<Teacher> list) {
                teacherMapper.batchInsert(list);
            }
        })
                .sheet()
                .doRead();
        return ResultResponse.success();
    }

    @Override
    public ResultResponse<String> headerRead() {
        // headerReadPath 这个地址下的 excel 中数据是多行头的  我们同意 StudentHeaderReadDTO 正确读取到了
        EasyExcelFactory.read(headerReadPath, StudentHeaderReadDTO.class, new StudentDtoExtendsListener<StudentHeaderReadDTO>(BATCH_COUNT, studentMapper) {
            @Override
            protected List<Student> convertClass(List<StudentHeaderReadDTO> list, int batchCount) {
                ArrayList<Student> studentList = new ArrayList<>();
                list.forEach(item->{
                    MapStructConvert instance = MapStructConvert.INSTANCE;
                    studentList.add(instance.studentHeaderReadDTO2Student(item));
                });
                return studentList;
            }
        })
                .sheet()
                .doRead();
        return ResultResponse.success();
    }


    /**
     * 获取一个 StudentDtoExtendsListener
     *
     * @return StudentDtoExtendsListener
     */
    private StudentDtoExtendsListener<StudentIndexOrNameReadDTO> getStudentDtoExtendsListener() {

        return new StudentDtoExtendsListener<StudentIndexOrNameReadDTO>(BATCH_COUNT, studentMapper) {
            @Override
            protected List<Student> convertClass(List<StudentIndexOrNameReadDTO> list, int batchCount) {
                ArrayList<Student> studentList = ListUtils.newArrayListWithExpectedSize(batchCount);
                list.forEach(studentIndexOrNameReadDTO -> {
                    MapStructConvert instance = MapStructConvert.INSTANCE;
                    studentList.add(instance.studentIndexOrNameReadDTO2Student(studentIndexOrNameReadDTO));
                });
                return studentList;
            }
        };
    }


}

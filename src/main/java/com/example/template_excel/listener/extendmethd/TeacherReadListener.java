package com.example.template_excel.listener.extendmethd;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.read.listener.ReadListener;
import com.alibaba.excel.util.ListUtils;

import java.util.List;

/**
 * @Author YangLi
 * @Date 2025/1/10 18:20
 * @注释
 */
public abstract class TeacherReadListener<T> implements ReadListener<T> {

    private static final int BATCH_COUNT = 100;

    private List<T> list = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);

    @Override
    public void invoke(T t, AnalysisContext analysisContext) {
        list.add(t);
        if (list.size() > BATCH_COUNT){
            saveData(list);
            list = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        saveData(list);
        list = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
    }

    protected abstract void saveData(List<T> list);
}

package com.example.template_excel.service;

import com.example.template_excel.response.ResultResponse;

/**
 * @Author YangLi
 * @Date 2024/12/26 11:15
 * @注释
 */
public interface ExcelWriteService {

    ResultResponse<String> simpleExport();

    ResultResponse<String> appointRowExport();

    ResultResponse<String> complexHeadExport();

    ResultResponse<String> repeatSameSheetExport();

    ResultResponse<String> repeatDifferentSheetExport();

    ResultResponse<String> converterDataExport();

    ResultResponse<String> exportExcel();

    ResultResponse<String> templateExport();

    ResultResponse<String> concurrentTemplateExportAnother();

    ResultResponse<String> actualCombatExport();

    ResultResponse<String> writeByCustomerStyle();

}

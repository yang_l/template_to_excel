package com.example.template_excel.response;

import com.alibaba.fastjson2.JSON;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.http.*;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Type;
import java.net.ConnectException;

/**
 * @description: 自定义异常处理
 * @author: YangLi
 * @date: 2024/11/20
 */
@EqualsAndHashCode(callSuper = true)
@ControllerAdvice
@Slf4j
@Data
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler implements ResponseBodyAdvice<Object>, RequestBodyAdvice {

    public GlobalExceptionHandler() {
        super();
    }

    private static final String JAVAX_ERROR = "javax.servlet.error.exception";

    private static final String REQUEST_URI = "请求的地址为： {}";

    private static final String XYZ = "*****>\n";

    /**
     * 处理404 的方式
     *
     * @param ex      `
     * @param body    `
     * @param headers `
     * @param status  `
     * @param request .
     * @return `
     */
    @Override
    @NonNull
    protected ResponseEntity<Object> handleExceptionInternal(@NonNull Exception ex,
                                                             Object body,
                                                             @NonNull HttpHeaders headers,
                                                             @NonNull HttpStatus status,
                                                             @NonNull WebRequest request) {
        if (HttpStatus.NOT_FOUND.equals(status)) {
            request.setAttribute(JAVAX_ERROR, ex, 0);
            return new ResponseEntity<>(new ResultResponse<>(ExceptionEnum.NOT_FOUND), headers, status);
        }
        if (HttpStatus.BAD_REQUEST.equals(status)) {
            request.setAttribute(JAVAX_ERROR, ex, 0);
            return new ResponseEntity<>(new ResultResponse<>(ExceptionEnum.BODY_NOT_MATCH), headers, status);
        }
        if (HttpStatus.UNSUPPORTED_MEDIA_TYPE.equals(status)) {
            request.setAttribute(JAVAX_ERROR, ex, 0);
            return new ResponseEntity<>(new ResultResponse<>(ExceptionEnum.UNSUPPORTED_MEDIA_TYPE), headers, status);
        }
        if (HttpStatus.METHOD_NOT_ALLOWED.equals(status)) {
            request.setAttribute(JAVAX_ERROR, ex, 0);
            return new ResponseEntity<>(new ResultResponse<>(ExceptionEnum.METHOD_NOT_ALLOWED), headers, status);
        }

        return new ResponseEntity<>(new ResultResponse<>(ExceptionEnum.INTERNAL_SERVER_ERROR), headers, status);
    }

    /**
     * 处理连接被拒异常
     *
     * @param e `
     * @return .
     */
    @ExceptionHandler(value = ConnectException.class)
    @ResponseBody
    public <T> ResultResponse<T> exceptionHandler(ConnectException e, HttpServletRequest request) {
        log.info(REQUEST_URI, request.getRequestURI());
        log.error("\n<*****连接被拒异常！原因是：*****>\n{}", e.getMessage());
        e.printStackTrace();
        return ResultResponse.error(ExceptionEnum.CONNECTION_REFUSED, request.getRequestURI());
    }

    /**
     * 处理空指针的异常
     *
     * @param e `
     * @return .
     */
    @ExceptionHandler(value = NullPointerException.class)
    @ResponseBody
    public <T> ResultResponse<T> exceptionHandler(NullPointerException e, HttpServletRequest request) {
        log.info(REQUEST_URI, request.getRequestURI());
        logger.error("\n<*****空指针异常！原因是:" + e.getMessage() + XYZ);
        e.printStackTrace();
        return ResultResponse.error(ExceptionEnum.NULL_POINTER_EXCEPTION, request.getRequestURI());
    }

    /**
     * 处理类型转换异常
     *
     * @param e `
     * @return .
     */
    @ExceptionHandler(value = NumberFormatException.class)
    @ResponseBody
    public <T> ResultResponse<T> exceptionHandler(NumberFormatException e, HttpServletRequest request) {
        log.info(REQUEST_URI, request.getRequestURI());
        logger.error("\n<*****类型转换异常！原因是:" + e.getMessage() + XYZ);
        e.printStackTrace();
        return ResultResponse.error(ExceptionEnum.PARAMS_NOT_CONVERT, request.getRequestURI());
    }

    @ExceptionHandler(value = ArithmeticException.class)
    @ResponseBody
    public ResultResponse<String> exceptionHandler(ArithmeticException e, HttpServletRequest request) {
        log.info(REQUEST_URI, request.getRequestURI());
        logger.error("\n<*****算数异常！原因是:" + e + XYZ);
        e.printStackTrace();
        return ResultResponse.error(ExceptionEnum.ARITHMETIC_EXCEPTION, e.getMessage(), request.getRequestURI());
    }

    /**
     * 处理空参数不合法
     *
     * @param e`
     * @return `
     */
    @ExceptionHandler(value = IllegalStateException.class)
    @ResponseBody
    public <T> ResultResponse<T> exceptionHandler(IllegalStateException e, HttpServletRequest request) {
        log.info(REQUEST_URI, request.getRequestURI());
        logger.error("\n<*****参数不合法！原因是:" + e.getMessage() + XYZ);
        e.printStackTrace();
        return ResultResponse.error(ExceptionEnum.ILLEGAL_STATE, request.getRequestURI());
    }

    /**
     * 处理自定义异常
     *
     * @param e `
     * @return `
     */
    @ExceptionHandler(value = BizException.class)
    @ResponseBody
    public <T> ResultResponse<T> exceptionHandler(BizException e, HttpServletRequest request) {
        log.info(REQUEST_URI, request.getRequestURI());
        logger.error("\n<*****自定义异常*****>！原因是:" + e.getMessage() + XYZ);
        e.printStackTrace();
        return ResultResponse.error(e.getMessage(), request.getRequestURI());
    }

    /**
     * 处理其他异常
     *
     * @param e ·
     * @return .
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public <T> ResultResponse<T> exceptionHandler(Exception e, HttpServletRequest request) {
        log.info(REQUEST_URI, request.getRequestURI());
        logger.error("\n<*****未知异常！原因是:" + e.getMessage() + XYZ);
        e.printStackTrace();
        return ResultResponse.error(ExceptionEnum.INTERNAL_SERVER_ERROR, request.getRequestURI());
    }

    @Override
    public boolean supports(@NonNull MethodParameter returnType, @NonNull Class<? extends HttpMessageConverter<?>> converterType) {
        /* 如需使用这个功能  需要将下面这个return true*/
        return false;
    }

    @Override
    public Object beforeBodyWrite(Object body, @NonNull MethodParameter returnType, @NonNull MediaType selectedContentType, @NonNull Class<? extends HttpMessageConverter<?>> selectedConverterType, @NonNull ServerHttpRequest request, @NonNull ServerHttpResponse response) {
        Object parse = null;
        if (body instanceof String) {
            parse = JSON.parse((String) body);
        }
        return parse;
    }

    @Override
    public boolean supports(@NonNull MethodParameter methodParameter, @NonNull Type targetType, @NonNull Class<? extends HttpMessageConverter<?>> converterType) {
        return false;
    }

    @Override
    @NonNull
    public HttpInputMessage beforeBodyRead(@NonNull HttpInputMessage httpInputMessage, @NonNull MethodParameter methodParameter, @NonNull Type type, @NonNull Class<? extends HttpMessageConverter<?>> aClass) {
        return httpInputMessage;
    }

    @Override
    @NonNull
    public Object afterBodyRead(@NonNull Object o, @NonNull HttpInputMessage httpInputMessage, @NonNull MethodParameter methodParameter, @NonNull Type type, @NonNull Class<? extends HttpMessageConverter<?>> aClass) {
        return httpInputMessage;
    }

    @Override
    public Object handleEmptyBody(Object o, @NonNull HttpInputMessage httpInputMessage, @NonNull MethodParameter methodParameter, @NonNull Type type, @NonNull Class<? extends HttpMessageConverter<?>> aClass) {
        return null;
    }
}

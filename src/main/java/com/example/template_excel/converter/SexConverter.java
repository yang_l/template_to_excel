package com.example.template_excel.converter;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.converters.ReadConverterContext;
import com.alibaba.excel.converters.WriteConverterContext;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.data.WriteCellData;

import java.util.Objects;

/**
 * @Author YangLi
 * @Date 2025/1/13 10:25
 * @注释
 */
public class SexConverter implements Converter<String> {

    @Override
    public Class<?> supportJavaTypeKey() {
        return String.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    /**
     * 这里读的时候会调用
     *
     * @param context `
     * @return `
     */
    @Override
    public String convertToJavaData(ReadConverterContext<?> context) {
        return Objects.equals(context.getReadCellData().getStringValue(), "男") ? "1" : "0";
    }

    /**
     * 这里是写的时候会调用 不用管
     *
     * @return `
     */
    @Override
    public WriteCellData<?> convertToExcelData(WriteConverterContext<String> context) {
        return new WriteCellData<>(context.getValue());
    }
}
